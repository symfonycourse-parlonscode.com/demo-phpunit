<?php

namespace App\Utils;

class Calculator
{
    public function add(int $a, int $b): int
    {
        return $a + $b;
    }

    public function substract(int $a, int $b): int
    {
        return $a - $b;
    }

    public function Multiplication(int $a, int $b): int
    {
        return $a * $b;
    }

    public function Divid(int $a, int $b): int
    {
        return $a / $b;
    }
}
