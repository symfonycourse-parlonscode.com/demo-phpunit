<?php

namespace App\Tests\Utils;

use App\Utils\Calculator;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

class CalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new Calculator;
        $result = $calculator->add(4, 8);
        $this->assertSame(12, $result);
    }

    public function testSubstract()
    {
        $calculator = new Calculator;
        $result = $calculator->substract(12, 3);
        $this->assertSame(9, $result);
    }

    public function testMultiplication()
    {
        $calculator = new Calculator;
        $result = $calculator->multiplication(2, 4);
        $this->assertSame(8, $result);
    }

    public function testDivid()
    {
        $calculator = new Calculator;
        $result = $calculator->divid(10, 2);
        $this->assertSame(5, $result);
    }
}

